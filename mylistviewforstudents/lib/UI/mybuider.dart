import 'package:flutter/material.dart';

class MyBuilder extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return MyBuilderState();
  }

}
class MyBuilderState extends State<MyBuilder>{

  final List<Map<String, dynamic>> _items = List.generate(
      100, (index) =>
      {"id": index, "title": "Item $index", "subtitle": "Subtitle $index"});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: _items.length,
        itemBuilder: (_, index) => Card(
      margin: const EdgeInsets.all(10),
      child: ListTile(
        title: Text(_items[index]['title']),
        subtitle: Text(_items[index]['subtitle']),
        trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            IconButton(onPressed: () {}, icon: const Icon(Icons.edit)),
            IconButton(onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(_items[index]["title"] + ' removed!'),
              ));
              setState((){
                final removed = _items.removeAt(index);
              });

            },
                icon: const Icon(Icons.delete)),
            IconButton(onPressed: () {}, icon: const Icon(Icons.add_box)),
          ],
        ),
      ),
    ),);
  }

}