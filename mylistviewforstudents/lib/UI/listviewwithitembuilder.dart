import 'package:flutter/material.dart';

class MyListViewWithItemBuilder extends StatelessWidget{
  final List myList = List.generate(1000, (index) {
    return {
      "id": index,
      "title": "This is the title $index",
      "subtitle": "This is the subtitle $index"
    };
  });

  MyListViewWithItemBuilder({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: myList.length,
      itemBuilder: (context, index) => Card(
        elevation: 6,
        margin: const EdgeInsets.all(10),
        child: ListTile(
          leading: CircleAvatar(
            child: Text(myList[index]["id"].toString()),
            backgroundColor: Colors.purple,
          ),
          title: Text(myList[index]["title"]),
          subtitle: Text(myList[index]["subtitle"]),
          trailing: const Icon(Icons.add_a_photo),
        ),
      ),
    );
  }

}