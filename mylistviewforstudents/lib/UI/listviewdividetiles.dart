import 'package:flutter/material.dart';

class ListViewDivideTiles extends StatelessWidget{

    final List<Map<String, dynamic>> _items = List.generate(
        10, (index) =>
        {"id": index, "title": "Item $index", "subtitle": "Subtitle $index"});

    ListViewDivideTiles({Key? key}) : super(key: key);

    @override
    Widget build(BuildContext context) {
      return ListView(
          children: ListTile.divideTiles(
              color: Colors.deepPurple,
              tiles: _items.map((item) => ListTile(
                leading: CircleAvatar(
                  backgroundColor: Colors.amber,
                  child: Text(item['id'].toString()),
                ),
                title: Text(item['title']),
                subtitle: Text(item['subtitle']),
                trailing: IconButton(
                  icon: Icon(Icons.delete),
                  onPressed: () {},
                ),
              ))).toList());
  }



}