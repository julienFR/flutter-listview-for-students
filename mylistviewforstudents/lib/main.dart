import 'package:flutter/material.dart';
import 'package:mylistviewforstudents/UI/listviewdividetiles.dart';
import 'package:mylistviewforstudents/UI/listviewhorizontale.dart';
import 'package:mylistviewforstudents/UI/listviewwithitembuilder.dart';
import 'package:mylistviewforstudents/UI/mybuider.dart';

import 'UI/MyListView.dart';


void main() {
  //runApp(const MyApp());
  runApp(MaterialApp(home:ListViewHorizontale()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ListView Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("ListView Demo"),
        ),
        body: ListTileTheme(
                contentPadding: const EdgeInsets.all(15),
                iconColor: Colors.red,
                textColor: Colors.black54,
                tileColor: Colors.yellow[100],
                style: ListTileStyle.list,
                dense: true,
                child: MyBuilder())
      ),
    );
  }
}




